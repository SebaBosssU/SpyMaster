define('two/spymaster', [
    'two/locale',
    'two/ready'
], function (
    Locale,
    ready
) {
    var AutoSpy = {}
    AutoSpy.version = '__spymaster_version'
    AutoSpy.init = function () {
        Locale.create('spymaster', __spymaster_locale, 'pl')

        AutoSpy.initialized = true
    }
    AutoSpy.run = function () {
        if (!AutoSpy.interfaceInitialized) {
            throw new Error('AutoSpy interface not initialized')
        }
        ready(function () {
            $player = modelDataService.getSelectedCharacter()
        }, ['initial_village'])
    }
    return AutoSpy
})