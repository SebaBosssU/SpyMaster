require([
    'two/ready',
    'two/spymaster',
    'two/spymaster/ui'
], function (
    ready,
    AutoSpy
) {
    if (AutoSpy.initialized) {
        return false
    }

    ready(function () {
        AutoSpy.init()
        AutoSpy.interface()
        AutoSpy.run()
    })
})
