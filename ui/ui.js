define('two/spymaster/ui', ['two/spymaster', 'two/locale', 'two/ui', 'two/ui/autoComplete', 'two/FrontButton', 'two/utils', 'two/eventQueue', 'ejs', 'struct/MapData', 'cdn'], function(AutoSpy, Locale, Interface, autoComplete, FrontButton, utils, eventQueue, ejs, $mapData, cdn) {
    var ui
    var opener
    var $window
    var $spyUnits
    var $spyBuildings
    var $spyEffective
    var $sabotage
    var $spyPlayer
    var $recruitSpies
    var $camouflage
    var $switch
    var $dummies
    var $exchange
    var $unitD
    var $unit
    var $replacement
    var $building
    /**
     * Bind all elements events and notifications.
     */
    var bindEvents = function() {
        $camouflage.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 3200
            var buildingT = $building[0].dataset.value
            var buildingLv = document.getElementById('level').value
            villages.forEach(function(village, index) {
                var data = village.data
                var buildings = data.buildings
                var tavern = buildings.tavern
                var level = tavern.level
                if (level >= 3) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.SCOUTING_SET_COUNTER_MEASURE, {
                            village_id: village.getId(),
                            type: 'camouflage',
                            status: 1,
                            building: buildingT,
                            level: buildingLv,
                            unit: '',
                            replacement: ''
                        })
                    }, index * interval * Math.random())
                }
            })
            utils.emitNotif('success', Locale('spymaster', 'camouflage.set'))
        })
        $switch.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            var unit = $unit[0].dataset.value
            var replacement = $replacement[0].dataset.value
            villages.forEach(function(village, index) {
                var data = village.data
                var buildings = data.buildings
                var tavern = buildings.tavern
                var level = tavern.level
                if (level >= 6) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.SCOUTING_SET_COUNTER_MEASURE, {
                            village_id: village.getId(),
                            type: 'switch_weapons',
                            status: 1,
                            building: '',
                            level: '',
                            unit: unit,
                            replacement: replacement
                        })
                    }, index * interval * Math.random())
                }
            })
            utils.emitNotif('success', Locale('spymaster', 'switch.set'))
        })
        $dummies.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            var unitD = $unitD[0].dataset.value
            villages.forEach(function(village, index) {
                var data = village.data
                var buildings = data.buildings
                var tavern = buildings.tavern
                var level = tavern.level
                if (level >= 9) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.SCOUTING_SET_COUNTER_MEASURE, {
                            village_id: village.getId(),
                            type: 'dummies',
                            status: 1,
                            building: '',
                            level: '',
                            unit: unitD,
                            replacement: ''
                        })
                    }, index * interval * Math.random())
                }
            })
            utils.emitNotif('success', Locale('spymaster', 'dummies.set'))
        })
        $exchange.on('click', function() {
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            villages.forEach(function(village, index) {
                var data = village.data
                var buildings = data.buildings
                var tavern = buildings.tavern
                var level = tavern.level
                if (level >= 12) {
                    setTimeout(function() {
                        socketService.emit(routeProvider.SCOUTING_SET_COUNTER_MEASURE, {
                            village_id: village.getId(),
                            type: 'exchange',
                            status: 1,
                            building: '',
                            level: '',
                            unit: '',
                            replacement: ''
                        })
                    }, index * interval * Math.random())
                }
            })
            utils.emitNotif('success', Locale('spymaster', 'exchange.set'))
        })
        $spyUnits.on('click', function() {
            var villageId = document.getElementById('SpU').value
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            var interval1 = 7000
            var Liczba = 0
            villages.forEach(function(village, index) {
                var scoutingInfo = village.scoutingInfo
                var spies = scoutingInfo.spies
                setTimeout(function() {
                    spies.forEach(function(available, index) {
                        if (available.type == 1) {
                            Liczba = Liczba + 1
                            if (Liczba <= 7) {
                                setTimeout(function() {
                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                        startVillage: village.getId(),
                                        targetVillage: villageId,
                                        spys: 1,
                                        type: 'units'
                                    })
                                }, index * interval * Math.random())
                                utils.emitNotif('success', 'Szpieg nr '+Liczba+' wysłany na '+villageId+'. Cel: jednostki')
                            }
                        }
                    })
                }, index * interval1 * Math.random())
            })
        })
        $spyBuildings.on('click', function() {
            var villageId = document.getElementById('SpB').value
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            var interval1 = 7000
            var Liczba = 0
            villages.forEach(function(village, index) {
                var scoutingInfo = village.scoutingInfo
                var spies = scoutingInfo.spies
                setTimeout(function() {
                    spies.forEach(function(available, index) {
                        if (available.type == 1) {
                            Liczba = Liczba + 1
                            if (Liczba <= 7) {
                                setTimeout(function() {
                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                        startVillage: village.getId(),
                                        targetVillage: villageId,
                                        spys: 1,
                                        type: 'buildings'
                                    })
                                }, index * interval * Math.random())
                                utils.emitNotif('success', 'Szpieg nr '+Liczba+' wysłany na '+villageId+'. Cel: budynki')
                            }
                        }
                    })
                }, index * interval1 * Math.random())
            })
        })
        $spyEffective.on('click', function() {
            var villageId = document.getElementById('SpA').value
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            var interval1 = 7000
            var Liczba = 0
            villages.forEach(function(village, index) {
                var scoutingInfo = village.scoutingInfo
                var spies = scoutingInfo.spies
                var numSpies = scoutingInfo.numSpies
                if (numSpies == 2) {
                    setTimeout(function() {
                        spies.forEach(function(available, index) {
                            if (available.type == 1) {
                                Liczba = Liczba + 1
                                if (Liczba <= 4) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                            startVillage: village.getId(),
                                            targetVillage: villageId,
                                            spys: 1,
                                            type: 'buildings'
                                        })
                                        setTimeout(function() {
                                            socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                startVillage: village.getId(),
                                                targetVillage: villageId,
                                                spys: 1,
                                                type: 'units'
                                            })
                                        }, index * interval)
                                    }, index * interval * Math.random())
                                    utils.emitNotif('success', 'Szpieg nr '+Liczba+' wysłany na '+villageId)
                                }
                            }
                        })
                    }, index * interval1 * Math.random())
                } else if (numSpies == 3) {
                    setTimeout(function() {
                        spies.forEach(function(available, index) {
                            if (available.type == 1) {
                                Liczba = Liczba + 1
                                if (Liczba <= 3) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                            startVillage: village.getId(),
                                            targetVillage: villageId,
                                            spys: 1,
                                            type: 'buildings'
                                        })
                                        console.log('Szpiegowanie budynków w ' + villageId)
                                        setTimeout(function() {
                                            socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                startVillage: village.getId(),
                                                targetVillage: villageId,
                                                spys: 1,
                                                type: 'buildings'
                                            })
                                            setTimeout(function() {
                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                    startVillage: village.getId(),
                                                    targetVillage: villageId,
                                                    spys: 1,
                                                    type: 'units'
                                                })
                                            }, index * interval)
                                        }, index * interval)
                                    }, index * interval * Math.random())
                                    utils.emitNotif('success', 'Szpieg nr '+Liczba+' wysłany na '+villageId)
                                }
                            }
                        })
                    }, index * interval1 * Math.random())
                } else if (numSpies == 4) {
                    setTimeout(function() {
                        spies.forEach(function(available, index) {
                            if (available.type == 1) {
                                Liczba = Liczba + 1
                                if (Liczba <= 2) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                            startVillage: village.getId(),
                                            targetVillage: villageId,
                                            spys: 1,
                                            type: 'buildings'
                                        })
                                        setTimeout(function() {
                                            socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                startVillage: village.getId(),
                                                targetVillage: villageId,
                                                spys: 1,
                                                type: 'buildings'
                                            })
                                            setTimeout(function() {
                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                    startVillage: village.getId(),
                                                    targetVillage: villageId,
                                                    spys: 1,
                                                    type: 'units'
                                                })
                                                setTimeout(function() {
                                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                        startVillage: village.getId(),
                                                        targetVillage: villageId,
                                                        spys: 1,
                                                        type: 'units'
                                                    })
                                                }, index * interval)
                                            }, index * interval)
                                        }, index * interval)
                                    }, index * interval * Math.random())
                                    utils.emitNotif('success', 'Szpieg nr '+Liczba+' wysłany na '+villageId)
                                }
                            }
                        })
                    }, index * interval1 * Math.random())
                } else if (numSpies == 5) {
                    setTimeout(function() {
                        spies.forEach(function(available, index) {
                            if (available.type == 1) {
                                Liczba = Liczba + 1
                                if (Liczba <= 2) {
                                    setTimeout(function() {
                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                            startVillage: village.getId(),
                                            targetVillage: villageId,
                                            spys: 1,
                                            type: 'buildings'
                                        })
                                        setTimeout(function() {
                                            socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                startVillage: village.getId(),
                                                targetVillage: villageId,
                                                spys: 1,
                                                type: 'buildings'
                                            })
                                            setTimeout(function() {
                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                    startVillage: village.getId(),
                                                    targetVillage: villageId,
                                                    spys: 1,
                                                    type: 'buildings'
                                                })
                                                setTimeout(function() {
                                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                        startVillage: village.getId(),
                                                        targetVillage: villageId,
                                                        spys: 1,
                                                        type: 'units'
                                                    })
                                                    setTimeout(function() {
                                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                            startVillage: village.getId(),
                                                            targetVillage: villageId,
                                                            spys: 1,
                                                            type: 'units'
                                                        })
                                                    }, index * interval)
                                                }, index * interval)
                                            }, index * interval)
                                        }, index * interval)
                                    }, index * interval * Math.random())
                                    utils.emitNotif('success', 'Szpieg nr '+Liczba+' wysłany na '+villageId)
                                }
                            }
                        })
                    }, index * interval1 * Math.random())
                }
            })
        })
        $sabotage.on('click', function() {
            var villageId = document.getElementById('sabotage').value
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            var interval1 = 7000
            var Liczba = 0
            villages.forEach(function(village, index) {
                var scoutingInfo = village.scoutingInfo
                var spies = scoutingInfo.spies
                var numSpies = scoutingInfo.numSpies
                setTimeout(function() {
                    spies.forEach(function(available, index) {
                        if (available.type == 1) {
                            if (numSpies >= 3) {
                                setTimeout(function() {
                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                        startVillage: village.getId(),
                                        targetVillage: villageId,
                                        spys: 3,
                                        type: 'sabotage'
                                    })
                                }, index * interval * Math.random())
                            }
                        }
                    })
                }, index * interval1 * Math.random())
                utils.emitNotif('success', 'Szpiedzy z wioski '+village.getId()+' sabotują wioskę '+villageId)
            })
        })
        $spyPlayer.on('click', function() {
            var playerId = document.getElementById('SpP').value
            var modelDataService = injector.get('modelDataService')
            var socketService = injector.get('socketService')
            var routeProvider = injector.get('routeProvider')
            var player = modelDataService.getSelectedCharacter()
            var villages = player.getVillageList()
            var interval = 5000
            var interval1 = 7000
            var Liczba = 0
            var targetVillages = []

            function targetList(id) {
                socketService.emit(routeProvider.CHAR_GET_PROFILE, {
                    character_id: playerId
                }, function(data) {
                    for (i = 0; i < data.villages.length; i++) {
                        targetVillages.push(data.villages[i].village_id)
                    }
                })
                setTimeout(spyPlayer, 3000)
            }

            function spyPlayer() {
                for (h = 0; h < targetVillages.length; h++) {
                    villages.forEach(function(village, index) {
                        var scoutingInfo = village.scoutingInfo
                        var spies = scoutingInfo.spies
                        var numSpies = scoutingInfo.numSpies
                        if (numSpies == 2) {
                            setTimeout(function() {
                                spies.forEach(function(available, index) {
                                    if (available.type == 1) {
                                        Liczba = Liczba + 1
                                        if (Liczba <= 4) {
                                            setTimeout(function() {
                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                    startVillage: village.getId(),
                                                    targetVillage: targetVillages[h],
                                                    spys: 1,
                                                    type: 'buildings'
                                                })
                                                console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                setTimeout(function() {
                                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                        startVillage: village.getId(),
                                                        targetVillage: targetVillages[h],
                                                        spys: 1,
                                                        type: 'units'
                                                    })
                                                    console.log('Szpiegowanie jednostek w ' + targetVillages[h])
                                                }, index * interval)
                                            }, index * interval * Math.random())
                                            console.log(Liczba)
                                        }
                                    }
                                })
                            }, index * interval1 * Math.random())
                        } else if (numSpies == 3) {
                            setTimeout(function() {
                                spies.forEach(function(available, index) {
                                    if (available.type == 1) {
                                        Liczba = Liczba + 1
                                        if (Liczba <= 3) {
                                            setTimeout(function() {
                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                    startVillage: village.getId(),
                                                    targetVillage: targetVillages[h],
                                                    spys: 1,
                                                    type: 'buildings'
                                                })
                                                console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                setTimeout(function() {
                                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                        startVillage: village.getId(),
                                                        targetVillage: targetVillages[h],
                                                        spys: 1,
                                                        type: 'buildings'
                                                    })
                                                    console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                    setTimeout(function() {
                                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                            startVillage: village.getId(),
                                                            targetVillage: targetVillages[h],
                                                            spys: 1,
                                                            type: 'units'
                                                        })
                                                        console.log('Szpiegowanie jednostek ' + targetVillages[h])
                                                    }, index * interval)
                                                }, index * interval)
                                            }, index * interval * Math.random())
                                            console.log(Liczba)
                                        }
                                    }
                                })
                            }, index * interval1 * Math.random())
                        } else if (numSpies == 4) {
                            setTimeout(function() {
                                spies.forEach(function(available, index) {
                                    if (available.type == 1) {
                                        Liczba = Liczba + 1
                                        if (Liczba <= 2) {
                                            setTimeout(function() {
                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                    startVillage: village.getId(),
                                                    targetVillage: targetVillages[h],
                                                    spys: 1,
                                                    type: 'buildings'
                                                })
                                                console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                setTimeout(function() {
                                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                        startVillage: village.getId(),
                                                        targetVillage: targetVillages[h],
                                                        spys: 1,
                                                        type: 'buildings'
                                                    })
                                                    console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                    setTimeout(function() {
                                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                            startVillage: village.getId(),
                                                            targetVillage: targetVillages[h],
                                                            spys: 1,
                                                            type: 'units'
                                                        })
                                                        console.log('Szpiegowanie jednostek w ' + targetVillages[h])
                                                        setTimeout(function() {
                                                            socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                                startVillage: village.getId(),
                                                                targetVillage: targetVillages[h],
                                                                spys: 1,
                                                                type: 'units'
                                                            })
                                                            console.log('Szpiegowanie jednostek w ' + targetVillages[h])
                                                        }, index * interval)
                                                    }, index * interval)
                                                }, index * interval)
                                            }, index * interval * Math.random())
                                            console.log(Liczba)
                                        }
                                    }
                                })
                            }, index * interval1 * Math.random())
                        } else if (numSpies == 5) {
                            setTimeout(function() {
                                spies.forEach(function(available, index) {
                                    if (available.type == 1) {
                                        Liczba = Liczba + 1
                                        if (Liczba <= 2) {
                                            setTimeout(function() {
                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                    startVillage: village.getId(),
                                                    targetVillage: targetVillages[h],
                                                    spys: 1,
                                                    type: 'buildings'
                                                })
                                                console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                setTimeout(function() {
                                                    socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                        startVillage: village.getId(),
                                                        targetVillage: targetVillages[h],
                                                        spys: 1,
                                                        type: 'buildings'
                                                    })
                                                    console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                    setTimeout(function() {
                                                        socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                            startVillage: village.getId(),
                                                            targetVillage: targetVillages[h],
                                                            spys: 1,
                                                            type: 'buildings'
                                                        })
                                                        console.log('Szpiegowanie budynków w ' + targetVillages[h])
                                                        setTimeout(function() {
                                                            socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                                startVillage: village.getId(),
                                                                targetVillage: targetVillages[h],
                                                                spys: 1,
                                                                type: 'units'
                                                            })
                                                            console.log('Szpiegowanie jednostek w ' + targetVillages[h])
                                                            setTimeout(function() {
                                                                socketService.emit(routeProvider.SCOUTING_SEND_COMMAND, {
                                                                    startVillage: village.getId(),
                                                                    targetVillage: targetVillages[h],
                                                                    spys: 1,
                                                                    type: 'units'
                                                                })
                                                                console.log('Szpiegowanie jednostek w ' + targetVillages[h])
                                                            }, index * interval)
                                                        }, index * interval)
                                                    }, index * interval)
                                                }, index * interval)
                                            }, index * interval * Math.random())
                                            console.log(Liczba)
                                        }
                                    }
                                })
                            }, index * interval1 * Math.random())
                        }
                    })
                }
            }
            targetList()
        })
        $recruitSpies.on('click', function() {
            function recruitSpy() {
                var player = modelDataService.getSelectedCharacter()
                var villages = player.getVillageList()
                villages.forEach(function(village) {
                    var data = village.data
                    var buildings = data.buildings
                    var tavern = buildings.tavern
                    var level = tavern.level
                    var scoutingInfo = village.scoutingInfo
                    var spies = scoutingInfo.spies
                    var resources = village.getResources()
                    var computed = resources.getComputed()
                    var maxStorage = resources.getMaxStorage()
                    var wood = computed.wood
                    var clay = computed.clay
                    var iron = computed.iron
                    var villageWood = wood.currentStock
                    var villageClay = clay.currentStock
                    var villageIron = iron.currentStock
                    var woodCost = [500, 1000, 2200, 7000, 12000]
                    var clayCost = [500, 800, 2000, 6500, 10000]
                    var ironCost = [500, 1200, 2400, 8000, 18000]
                    if (level < 1) {} else if (level >= 1 && level < 3) {
                        spies.forEach(function(spy) {
                            if (spy.id == 1 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[1] && villageClay >= clayCost[1] && villageIron >= ironCost[1])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                            }
                        })
                        console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                    } else if (level >= 3 && level < 6) {
                        spies.forEach(function(spy) {
                            if (spy.id == 1 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[1] && villageClay >= clayCost[1] && villageIron >= ironCost[1])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 2 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[2] && villageClay >= clayCost[2] && villageIron >= ironCost[2])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            }
                        })
                    } else if (level >= 6 && level < 9) {
                        spies.forEach(function(spy) {
                            if ((spy.id == 3 && spy.recruitingInProgress == true) && (spy.id == 1 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 1 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[1] && villageClay >= clayCost[1] && villageIron >= ironCost[1])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 2 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[2] && villageClay >= clayCost[2] && villageIron >= ironCost[2])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 3 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[3] && villageClay >= clayCost[3] && villageIron >= ironCost[3])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'can be recruited')
                            }
                        })
                    } else if (level >= 9 && level < 12) {
                        spies.forEach(function(spy) {
                            if ((spy.id == 4 && spy.recruitingInProgress == true) && (spy.id == 1 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 4, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 4 && spy.recruitingInProgress == true) && (spy.id == 2 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 4, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 3 && spy.recruitingInProgress == true) && (spy.id == 1 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 3 && spy.recruitingInProgress == true) && (spy.id == 2 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 1 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[1] && villageClay >= clayCost[1] && villageIron >= ironCost[1])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 2 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[2] && villageClay >= clayCost[2] && villageIron >= ironCost[2])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 3 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[3] && villageClay >= clayCost[3] && villageIron >= ironCost[3])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 4 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[4] && villageClay >= clayCost[4] && villageIron >= ironCost[4])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                console.log('spy', 4, 'from village', village.getId(), 'can be recruited')
                            }
                        })
                    } else if (level >= 12) {
                        spies.forEach(function(spy) {
                            if ((spy.id == 5 && spy.recruitingInProgress == true) && (spy.id == 1 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 5
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 5, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 5 && spy.recruitingInProgress == true) && (spy.id == 2 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 5
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 5, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 4 && spy.recruitingInProgress == true) && (spy.id == 1 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 4, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 3 && spy.recruitingInProgress == true) && (spy.id == 1 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 4 && spy.recruitingInProgress == true) && (spy.id == 2 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 4, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 5 && spy.recruitingInProgress == true) && (spy.id == 3 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 5
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                console.log('spy', 5, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 3, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 3 && spy.recruitingInProgress == true) && (spy.id == 2 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 4 && spy.recruitingInProgress == true) && (spy.id == 3 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                console.log('spy', 4, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 3, 'from village', village.getId(), 'can be recruited')
                            } else if ((spy.id == 5 && spy.recruitingInProgress == true) && (spy.id == 4 && spy.active != true)) {
                                socketService.emit(routeProvider.SCOUTING_CANCEL_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 5
                                })
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                console.log('spy', 5, 'from village', village.getId(), 'recruit cancelled')
                                console.log('spy', 4, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 1 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[1] && villageClay >= clayCost[1] && villageIron >= ironCost[1])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 1
                                })
                                console.log('spy', 1, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 2 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[2] && villageClay >= clayCost[2] && villageIron >= ironCost[2])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 2
                                })
                                console.log('spy', 2, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 3 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[3] && villageClay >= clayCost[3] && villageIron >= ironCost[3])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 3
                                })
                                console.log('spy', 3, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 4 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[4] && villageClay >= clayCost[4] && villageIron >= ironCost[4])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 4
                                })
                                console.log('spy', 4, 'from village', village.getId(), 'can be recruited')
                            } else if (spy.id == 5 && spy.active != true && spy.recruitingInProgress != true && (villageWood >= woodCost[5] && villageClay >= clayCost[5] && villageIron >= ironCost[5])) {
                                socketService.emit(routeProvider.SCOUTING_RECRUIT, {
                                    village_id: village.getId(),
                                    slot: 5
                                })
                                console.log('spy', 5, 'from village', village.getId(), 'can be recruited')
                            }
                        })
                    }
                })
            }
            utils.emitNotif('success', Locale('spymaster', 'revived'))
            recruitSpy()
        })
        $unitD.on('selectSelected', function() {
	        unitD = $unitD[0].dataset.value
        })
        $unit.on('selectSelected', function() {
	        unitD = $unit[0].dataset.value
        })
        $replacement.on('selectSelected', function() {
	        unitD = $replacement[0].dataset.value
        })
        $building.on('selectSelected', function() {
	        unitD = $building[0].dataset.value
        })
    }
    var SpymasterInterface = function() {
        ui = new Interface('AutoSpy', {
            activeTab: 'spy',
            template: '__spymaster_html_window',
            css: '__spymaster_css_style',
            replaces: {
                locale: Locale,
                version: AutoSpy.version,
                types: ['village', 'character']
            }
        })
        opener = new FrontButton(Locale('spymaster', 'title'), {
            classHover: false,
            classBlur: false,
            onClick: function() {
                ui.openWindow()
            }
        })
        $window = $(ui.$window)
        $spyUnits = $window.find('.item-send span')
        $spyBuildings = $window.find('.item-send1 span')
        $spyEffective = $window.find('.item-send2 span')
        $sabotage = $window.find('.item-send3 span')
        $spyPlayer = $window.find('.item-send4 span')
        $recruitSpies = $window.find('.item-recruit span')
        $camouflage = $window.find('.item-camouflage span')
        $switch = $window.find('.item-switch span')
        $dummies = $window.find('.item-dummies span')
        $exchange = $window.find('.item-exchange span')
        $unitD = $window.find('.dataDummies')
        $unit = $window.find('.dataUnit')
        $replacement = $window.find('.dataReplacement')
        $building = $window.find('.dataBuilding')		
        bindEvents()
        AutoSpy.interfaceInitialized = true
        return ui
    }
    AutoSpy.interface = function() {
        AutoSpy.interface = SpymasterInterface()
    }
})